package arb;


public class Arbitrage  {

    public static void main (String[] Argz){
        int V = StdIn.readInt();
        String[] name = new String[V];
        EdgeWeightedDigraph G = new EdgeWeightedDigraph(V);
        for (int v = 0; v < V; v++) {
            name[v] = StdIn.readString();
            for (int w = 0; w < V ; w++) {

                Double Rate = StdIn.readDouble();
                DirectedEdge e = new DirectedEdge ( v, w, -Math.log(Rate));
                G.addEdge(e);

            }
        }

        BellmanFordSP spt = new BellmanFordSP ( G,0);
        if (spt.hasNegativeCycle())
        {
            Double stake = 1000.0;
            for (DirectedEdge e: spt.NegativeCycle())
            {
               StdOut.printf("%10.5f",stake,name[e.from()]);
                stake *= Math.exp(-e.weight());
                StdOut.printf(" = %10.5f %s\n",stake,name[e.to()]);
            }

        }
        else System.out.println("нет возможности арбитража");
    }


}
